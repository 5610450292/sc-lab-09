package tree;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal{

	ArrayList<Node> inOrder = new ArrayList<Node>();
	@Override
	public ArrayList<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		
		Node x = node;
		  if(x == null){ return null;}
		  
		  
		  traverse( x.getLeft() );
		  inOrder.add(x);
		  traverse( x.getRight() ); 
		
		return inOrder;
	}
}
