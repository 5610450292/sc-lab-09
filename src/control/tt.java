package control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import model.*;

public class tt {
	
	public static void main(String[] args){
		//Test Person
		System.out.println("TEST PERSON");
		ArrayList<Person> persons = new  ArrayList<Person>();
		persons.add(new Person("aa",180,500000));
		persons.add(new Person("bb",185,100000));
		persons.add(new Person("cc",175,200000));
		
		Comparator<Person> heightComparator = new HeightComparator();
		//Comparator<Person> incomeComparator = new HeightComparator();
		
		Collections.sort(persons, heightComparator);
		
		for (Person p : persons) {
			System.out.println(p);
		}
		//Test Product
		System.out.println("TEST PRODUCT");
		ArrayList<Product> product = new ArrayList<Product>() ;
		product.add(new Product("aa",100)) ;
		product.add(new Product("bb",300)) ;
		product.add(new Product("cc",1500)) ;
		product.add(new Product("dd",250)) ;
		Comparator<Product> PriceComparator = new PriceComparator();
		Collections.sort(product,PriceComparator);
		for (Product p : product){
			System.out.println(p) ;
		}
		//TEST COMPANY
		System.out.println("TEST COMPANY");
		ArrayList<Company> company = new ArrayList<Company>() ;
		company.add(new Company("aa",100,50,50)) ;
		company.add(new Company("bb",300,25,275)) ;
		company.add(new Company("cc",1500,100,500)) ;
		company.add(new Company("dd",250,100,150)) ;
		System.out.println("Income Sort");
		Comparator<Company> EarningComparator = new EarningComparator();
		Collections.sort(company,EarningComparator);
		for (Company c : company){
			System.out.println(c) ;
		}
		System.out.println("Payout Sort");
		Comparator<Company> ExpenseComparator = new ExpenseComparator();
		Collections.sort(company,ExpenseComparator);
		for (Company c : company){
			System.out.println(c) ;
		}
		System.out.println("Profit Sort");
		Comparator<Company> ProfitComparator = new ProfitComparator();
		Collections.sort(company,ProfitComparator);
		for (Company c : company){
			System.out.println(c) ;
		}
		//TEST TAX
		System.out.println("TEST TAX");
		ArrayList<Tax> tax = new ArrayList<Tax>() ;
		Person person1 = new Person("aa",150,155) ;
		Product product1 = new Product("bb",600) ;
		Company company1 = new Company("cc",2000,1500,500) ;
		tax.add(new Tax(person1 ,product1,company1)) ;
		tax.add(new Tax(person1 ,product1,company1)) ;
		tax.add(new Tax(person1 ,product1,company1)) ;
		/*tax.add(new Tax("bb",300)) ;
		Tax.add(new Tax("cc",1500)) ;
		Tax.add(new Tax("dd",250)) ;*/
		Comparator<Tax> TaxComparator = new TaxComparator();
		Collections.sort(tax,TaxComparator);
		for (Tax t : tax){
			System.out.println(t) ;
		}
	}

}
