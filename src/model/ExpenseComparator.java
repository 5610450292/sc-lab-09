package model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		// TODO Auto-generated method stub
		if (o1.getPayout() < o2.getPayout()){return -1 ; }
		else if (o1.getPayout() > o2.getPayout()){return 1 ; }
		else {return 0;}
	}
	

}
