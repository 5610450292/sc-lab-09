package model;
import model.* ;
public class Tax {
	Person person  ;
	Product product ;
	Company company ;
	private int i = 1 ;
	public Tax (Person person,Product product,Company company){
		this.person = person ;
		this.product = product ;
		this.company = company ;
		
	}
	public double getTax(){
		
		if (i%3==1){
			i++ ;
			return person.getTax() ;}
		else if (i%3==2){
			i++;
			return product.getTax() ; }
		else {
			i++ ; 
			return company.getTax() ; }
	}
	public String toString(){
		
		if (i%3 == 1 ){
			i++;
			return "Name : " + person.getName() + " , Tax : " + person.getTax() ;}
		else if (i%3==2){
			i++;
			return "Name : " + product.getName() + " , Tax : " + product.getTax() ;}
		else {
			return "Name : " + company.getName() + " , Tax : " + company.getTax() ;}
		
		
	}

	
}
