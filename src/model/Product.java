package model;

import interfaces.Taxable;

public class Product implements Taxable{
	
	String name;
	double price;
	
	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}
	public String getName(){
		return this.name ;
	}
	public double getPrice () {
		return this.price ;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return price * 0.07;
	}
	public String toString(){
		return "Name : " + this.getName() + " , Price : " + this.getPrice() 
			+ " , Tax : " + this.getTax();
	}

}
