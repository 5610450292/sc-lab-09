package model;

import java.util.Comparator;

import interfaces.Taxable;

public class Company implements Taxable {
	
	String name;
	double income;
	double payout;
	double profit ;

	public Company(String name, double income, double payout ,double profit) {
		this.name = name;
		this.income = income;
		this.payout = payout;
		this.profit = profit ;
	}
	public String getName() {
		return this.name ;
	}
	public double getIncome(){
		return this.income ;
	}
	public double getPayout(){
		return this.payout ;
	}
	public double getProfit(){
		return this.profit ;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0;
		if (income - payout >= 0) {
			tax = (income - payout) * 0.3;
		}
		return tax;
	}
	public String toString(){
		return "Name : " + this.name + " , Income : " + this.getIncome() 
				+ " , Payout : " + this.getPayout() + " , Profit : "+ this.getProfit()+" , Tax : " + this.getTax();
	}

}
