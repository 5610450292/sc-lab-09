package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable,Taxable,Comparable{

	String name ;
	double height;
	double salary;	
	
	public Person(String name, double height, double salary) {
		this.name = name;
		this.height = height;
		this.salary = salary;
	}
	public String getName(){
		return this.name;
	}
	public double getHeight(){
		return this.height;
	}
	public double getSalary(){
		return this.salary;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.height;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0;
		double annual = 0;
		if (Math.floor(salary) <= 300000){
			tax = this.salary * 0.05;
		}
		else {
			tax = 300000 * 0.05;
			annual = this.salary - 300000;
			tax = tax + (annual * 0.1);
		}
		return tax;
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Person other = (Person) obj;
		if (this.getSalary() < other.getSalary()) {
			return -1;
		}
		else if (this.getSalary() > other.getSalary()){
			return 1 ;
		}
		else {
			return 0;
		}
	}
	public String toString(){
		return "Name : " + this.getName() + " , Height : " + this.getHeight() 
				+ " , Yearly Incom : " + this.getSalary() + " , Tax : " + this.getTax();
	}
	

}
